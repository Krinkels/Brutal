/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
#ifndef XMALLOC_H
#define XMALLOC_H

#ifndef DEBUG_MALLOC

#define xmalloc  checking_malloc
#define xmalloc0 checking_malloc0
#define xrealloc checking_realloc
#define xstrdup  checking_strdup
#define xfree    checking_free

void	*checking_malloc( size_t size );
void	*checking_malloc0( size_t size );
void	*checking_realloc( void *ptr, size_t newsize );
char	*checking_strdup( const char *s );
void	checking_free( void *ptr );

#define InitDebugMemory()
#define DestroyDebugMemory()

#else  /* DEBUG_MALLOC */

#define xmalloc(s)     debugging_malloc (s, __FILE__, __LINE__)
#define xmalloc0(s)    debugging_malloc0 (s, __FILE__, __LINE__)
#define xrealloc(p, s) debugging_realloc (p, s, __FILE__, __LINE__)
#define xstrdup(p)     debugging_strdup (p, __FILE__, __LINE__)
#define xfree(p)       debugging_free (p, __FILE__, __LINE__)

void	*debugging_malloc( size_t size, const char *source_file, int source_line );
void	*debugging_malloc0( size_t size, const char *source_file, int source_line );
void	*debugging_realloc( void *ptr, size_t newsize, const char *source_file, int source_line );
char	*debugging_strdup( const char *s, const char *source_file, int source_line );
void 	debugging_free( void *ptr, const char *source_file, int source_line );

void	InitDebugMemory( void );
void	DestroyDebugMemory( void );

#endif /* DEBUG_MALLOC */

void	print_malloc_debug_stats( void );


#endif /* XMALLOC_H */