#pragma once
#include <windows.h>
#include <gdiplus.h>

//#define cmnMouseClickEvent 1

using namespace Gdiplus;
using namespace Gdiplus::DllExports;

typedef void( *callback_t )( void );
typedef void*( __stdcall *GDIConstructor_f )( void );
typedef int( __stdcall *ImgLoad_f )( HWND Wnd, GpImage *Img, int Left, int Top, int Width, int Height, BOOL Stretch, BOOL IsBkg );
typedef HWND( __stdcall *BtnCreate_f )( HWND Wnd, int Left, int Top, int Width, int Height, GpImage *Img, int ShadowWidth, BOOL IsCheckBtn );
typedef void( __stdcall *BtnSetEvent_f )( HWND Wnd, int EventID, callback_t Event );
typedef void( __stdcall *ImgApplyChanges_f )( HWND Wnd );
typedef void( __stdcall *CreateFormFromImage_f )( HWND Wnd, GpImage *Img );
typedef void( __stdcall *BtnSetVisibility_f )( HWND Wnd, BOOL Value );
typedef void( __stdcall *ImgRelease_f )( int Img );
typedef void( __stdcall *ImgSetTransparent_f )( int Img, int Value );
typedef void( __stdcall *gdipShutdown_f )( void );


extern GDIConstructor_f GDIConstructorb;
extern ImgLoad_f ImgLoad;
extern BtnCreate_f BtnCreate;
extern BtnSetEvent_f BtnSetEvent;
extern ImgApplyChanges_f ImgApplyChanges;
extern gdipShutdown_f gdipShutdown;
extern CreateFormFromImage_f CreateFormFromImag;
extern BtnSetVisibility_f BtnSetVisibility;
extern ImgSetTransparent_f ImgSetTransparent;
extern ImgRelease_f ImgRelease;
//////////////////////////////////////////////////////////////////////////

bool BotvaInit( void );
void BotvaDeInit( void );