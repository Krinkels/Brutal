﻿/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/

#include <ctgmath>
#include <float.h>
#include <vector>
#include "images.h"
#include "xmalloc.h"

#define k_abs(a, b) fabs( a - b ) <= FLT_EPSILON * fmax( fabs( a ), fabs( b ) )

#define bsEnabled 0x0001
#define bsFocused 0x0010
#define bsPressed 0x0100
#define bsChecked 0x1000

//TgdipButton btn = { 0 };
std::vector<HWND>wndbtn;
TgdipDialog *Dialog;
TgdipButton *btn = nullptr;

void DeleteButton( void );

//////////////////////////////////////////////////////////////////////////
void CMInit( ColorMatrix *cm )
{
	for( int i = 0; i < 4; ++i )
	{
		for( int j = 0; j < 4; ++j )
		{
			if( i == j )
				cm->m[ i ][ j ] = 1;
			else
				cm->m[ i ][ j ] = 0;
		}
	}
}

DWORD GetFormColor( void )
{
	HDC MemDC;
	HBITMAP BMP;
	HGDIOBJ oldBMP;
	DWORD Clr;
	DWORD r, g, b;
	int RedShift = 16;
	int GreenShift = 8;
	int BlueShift = 0;


	MemDC = CreateCompatibleDC( Dialog->hDCMem );
	BMP = CreateCompatibleBitmap( Dialog->hDCMem, 1, 1 );
	oldBMP = SelectObject( MemDC, BMP );
	CallWindowProc( Dialog->OldProc, Dialog->Wnd, WM_ERASEBKGND, ( WPARAM )MemDC, 0 );

	Clr = GetPixel( MemDC, 0, 0 );
	SelectObject( MemDC, oldBMP );
	DeleteDC( MemDC );
	DeleteObject( BMP );

	b = Clr >> RedShift;
	Clr = Clr - ( b << RedShift );
	g = Clr >> GreenShift;
	Clr = Clr - ( g << GreenShift );
	r = Clr >> BlueShift;

	return ( DWORD( b ) << BlueShift ) | ( DWORD( g ) << GreenShift ) | ( DWORD( r ) << RedShift );
}

BOOL IsImgStretched( int k11, int k12, int k21, int k22 )
{
	return ( k11 != k12 ) || ( k21 != k22 );
}

void SetRectF( TRectF *rect, float Left, float Top, float Right, float Bottom )
{
	rect->Left = Left;
	rect->Top = Top;
	rect->Right = Right;
	rect->Bottom = Bottom;
}

BOOL IsRectFEmpty( TRectF rect )
{
	return ( ( rect.Right - rect.Left ) == 0 ) || ( ( rect.Bottom - rect.Top ) == 0 );
}

void SetFullImage( BOOL IsBkg, RECT UpdRect )
{
	GpBitmap *m_img;
	GpGraphics *pGraphics = { 0 };
	DWORD BkgColor;
	ColorMatrix cm;
	GpImageAttributes *ImgAttr;
	BOOL IsEmptyLayer;
	PImg *cimg;
	RECT ImgRect, DrawRect, SrcRect, DstRect;
	float kw, kh;
	TRectF SrcRectF = { 0 }, DstRectF = { 0 };


	if( IsBkg )
		m_img = Dialog->BImg;
	else
		m_img = Dialog->FImg;

	if( ( Dialog->PFirstImg != NULL ) || IsBkg )
	{
		if( m_img == NULL )
			CreateEmptyImage( &m_img, Dialog->Wnd );

		pGraphics = NULL;
		GdipGetImageGraphicsContext( m_img, &pGraphics );
		GdipSetSmoothingMode( pGraphics, SmoothingModeHighSpeed );
		GdipSetInterpolationMode( pGraphics, InterpolationModeHighQualityBilinear );

		if( IsBkg )
			BkgColor = 0xFF000000 | GetFormColor();
		else
			BkgColor = 0;

		GdipSetClipRectI( pGraphics, UpdRect.left, UpdRect.top, UpdRect.right - UpdRect.left, UpdRect.bottom - UpdRect.top, CombineModeReplace );

		GdipGraphicsClear( pGraphics, BkgColor );
		GdipResetClip( pGraphics );

		CMInit( &cm );
		GdipCreateImageAttributes( &ImgAttr );

		IsEmptyLayer = TRUE;

		cimg = Dialog->PFirstImg;
		while( cimg != NULL )
		{
			if( ( cimg->IsBkg == IsBkg ) && ( cimg->Visible ) && ( cimg->Transparent > 0 ) && ( cimg->VisibleRect.right > 0 ) && ( cimg->VisibleRect.bottom > 0 ) )
			{
				if( IsEmptyLayer )
					IsEmptyLayer = FALSE;

				SetRect( &ImgRect, cimg->Left, cimg->Top, cimg->Left + cimg->Width, cimg->Top + cimg->Height );

				if( IntersectRect( &DrawRect, &ImgRect, &UpdRect ) )
				{
					if( IsImgStretched( cimg->Width, cimg->VisibleRect.right, cimg->Height, cimg->VisibleRect.bottom ) == FALSE )
					{
						SetRect( &SrcRect,
							( DrawRect.left - cimg->Left ) + cimg->VisibleRect.left,
								 ( DrawRect.top - cimg->Top ) + cimg->VisibleRect.top,
								 ( DrawRect.right - cimg->Left ) + cimg->VisibleRect.left,
								 ( DrawRect.bottom - cimg->Top ) + cimg->VisibleRect.top );
						if( IsRectEmpty( &SrcRect ) == FALSE )
						{
							SetRect( &SrcRect, SrcRect.left, SrcRect.top, SrcRect.right - SrcRect.left, SrcRect.bottom - SrcRect.top );
							SetRect( &DstRect, DrawRect.left, DrawRect.top, DrawRect.right - DrawRect.left, DrawRect.bottom - DrawRect.top );
							//if( cimg->Transparent != 255 )
							if( k_abs( cimg->Transparent, 255 ) == FALSE )
							{
								cm.m[ 3 ][ 3 ] = cimg->Transparent / 255;
								GdipSetImageAttributesColorMatrix( ImgAttr, ColorAdjustTypeDefault, TRUE, &cm, NULL, ColorMatrixFlagsDefault );
								GdipDrawImageRectRectI(	pGraphics,
														cimg->Image,
														DstRect.left,
														DstRect.top,
														DstRect.right,
														DstRect.bottom,
														SrcRect.left,
														SrcRect.top,
														SrcRect.right,
														SrcRect.bottom,
														UnitPixel,
														ImgAttr,
														NULL,
														NULL );
							}
							else
								GdipDrawImageRectRectI(	pGraphics,
														cimg->Image,
														DstRect.left,
														DstRect.top,
														DstRect.right,
														DstRect.bottom,
														SrcRect.left,
														SrcRect.top,
														SrcRect.right,
														SrcRect.bottom,
														UnitPixel,
														NULL,
														NULL,
														NULL );
						}
					}
					else
					{
						kw = ( float )cimg->Width / cimg->VisibleRect.right;
						kh = ( float )cimg->Height / cimg->VisibleRect.bottom;

						SetRectF( &SrcRectF,
							( DrawRect.left - cimg->Left ) / kw + cimg->VisibleRect.left,
								  ( DrawRect.top - cimg->Top ) / kh + cimg->VisibleRect.top,
								  ( DrawRect.right - cimg->Left ) / kw + cimg->VisibleRect.left,
								  ( DrawRect.bottom - cimg->Top ) / kh + cimg->VisibleRect.top );

						if( IsRectFEmpty( SrcRectF ) == FALSE )
						{
							SetRectF( &SrcRectF, SrcRectF.Left, SrcRectF.Top, SrcRectF.Right - SrcRectF.Left, SrcRectF.Bottom - SrcRectF.Top );
							SetRectF( &DstRectF, DrawRect.left, DrawRect.top, DrawRect.right - DrawRect.left, DrawRect.bottom - DrawRect.top );

							//if( cimg->Transparent != 255 )
							if( k_abs( cimg->Transparent, 255 ) == FALSE )
							{
								cm.m[ 3 ][ 3 ] = cimg->Transparent / 255;
								GdipSetImageAttributesColorMatrix( ImgAttr, ColorAdjustTypeDefault, TRUE, &cm, NULL, ColorMatrixFlagsDefault );
								GdipDrawImageRectRect(	pGraphics,
														cimg->Image,
														DstRectF.Left,
														DstRectF.Top,
														DstRectF.Right,
														DstRectF.Bottom,
														SrcRectF.Left,
														SrcRectF.Top,
														SrcRectF.Right,
														SrcRectF.Bottom,
														UnitPixel,
														ImgAttr,
														NULL,
														NULL );
							}
							else
								GdipDrawImageRectRect(	pGraphics,
														cimg->Image,
														DstRectF.Left,
														DstRectF.Top,
														DstRectF.Right,
														DstRectF.Bottom,
														SrcRectF.Left,
														SrcRectF.Top,
														SrcRectF.Right,
														SrcRectF.Bottom,
														UnitPixel,
														NULL,
														NULL,
														NULL );
						}
					}
				}
			}
			cimg = cimg->PNextImg;
		}

		if( !IsBkg && IsEmptyLayer )
		{
			if( m_img != NULL )
				GdipDisposeImage( m_img );

			m_img = NULL;
		}

		GdipDisposeImageAttributes( ImgAttr );
	}
	else
	{
		if( m_img != NULL )
			GdipDisposeImage( m_img );

		m_img = NULL;
	}

	if( IsBkg )
		Dialog->BImg = m_img;
	else
		Dialog->FImg = m_img;

	GdipDeleteGraphics( pGraphics );
}

void DrawFormToDCMem( RECT UpdateRect )
{
	RECT r;
	HDC mdc;
	HBITMAP bmp;
	HGDIOBJ oldbmp;
	GpGraphics *pGraphics;

	GetClientRect( Dialog->Wnd, &r );
	mdc = CreateCompatibleDC( Dialog->hDCMem );
	bmp = CreateCompatibleBitmap( Dialog->hDCMem, r.right, r.bottom );
	oldbmp = SelectObject( mdc, bmp );

	pGraphics = NULL;
	GdipCreateFromHDC( mdc, &pGraphics );

	if( Dialog->BImg != NULL )
		GdipDrawImageRectRectI(	pGraphics,
								Dialog->BImg,
								UpdateRect.left,
								UpdateRect.top,
								UpdateRect.right - UpdateRect.left,
								UpdateRect.bottom - UpdateRect.top,
								UpdateRect.left,
								UpdateRect.top,
								UpdateRect.right - UpdateRect.left,
								UpdateRect.bottom - UpdateRect.top,
								UnitPixel,
								NULL,
								NULL,
								NULL );
	CallWindowProc( Dialog->OldProc, Dialog->Wnd, WM_PAINT, ( WPARAM )mdc, 0 );

	if( Dialog->FImg != NULL )
		GdipDrawImageRectRectI( pGraphics,
								Dialog->FImg,
								UpdateRect.left,
								UpdateRect.top,
								UpdateRect.right - UpdateRect.left,
								UpdateRect.bottom - UpdateRect.top,
								UpdateRect.left,
								UpdateRect.top,
								UpdateRect.right - UpdateRect.left,
								UpdateRect.bottom - UpdateRect.top,
								UnitPixel,
								NULL,
								NULL,
								NULL );

	GdipDeleteGraphics( pGraphics );

	BitBlt( Dialog->hDCMem,
			UpdateRect.left,
			UpdateRect.top,
			UpdateRect.right - UpdateRect.left,
			UpdateRect.bottom - UpdateRect.top,
			mdc,
			UpdateRect.left,
			UpdateRect.top,
			SRCCOPY );

	SelectObject( mdc, oldbmp );
	DeleteDC( mdc );
	DeleteObject( bmp );
}

void DeleteImage( PImg *pimg )
{
	pimg->Delete = TRUE;

	if( pimg->Image != NULL )
	{
		GdipDisposeImage( pimg->Image );
		pimg->Image = NULL;
	}

	if( pimg->PNextImg != NULL )
		pimg->PNextImg->PPrevImg = pimg->PPrevImg;
	else
		Dialog->PLastImg = pimg->PPrevImg;

	if( pimg->PPrevImg != NULL )
		pimg->PPrevImg->PNextImg = pimg->PNextImg;
	else
		Dialog->PFirstImg = pimg->PNextImg;

	xfree( pimg );
}

void DeleteWnd( void )
{
	//	int Last;
	SetWindowLong( Dialog->Wnd, GWL_WNDPROC, ( LONG )Dialog->OldProc );
	if( Dialog->BImg != NULL )
	{
		GdipDisposeImage( Dialog->BImg );
		Dialog->BImg = NULL;
	}

	if( Dialog->FImg != NULL )
	{
		GdipDisposeImage( Dialog->FImg );
		Dialog->FImg = NULL;
	}

	SelectObject( Dialog->hDCMem, Dialog->hOld );
	DeleteObject( Dialog->hBmp );
	DeleteDC( Dialog->hDCMem );

	xfree( Dialog );
}

static LRESULT CALLBACK WndProc( HWND Wnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	switch( Msg )
	{
		case WM_ERASEBKGND:
		{
			return 1;
		}

		case WM_PAINT:
		{
			HDC DC;
			PAINTSTRUCT ps;
			RECT r;

			if( ( HDC )wParam == 0 )
			{
				DC = BeginPaint( Wnd, &ps );
				DrawFormToDCMem( ps.rcPaint );

				//if( AWnd[ k ].RefreshBtn )
				if( Dialog->RefreshBtn == TRUE )
				{
					//EnumChildWindows( Wnd, ( WNDENUMPROC )RefreshChildWnd, ( LPARAM )&ps.rcPaint );
					//AWnd[ k ].RefreshBtn = FALSE;
					//InvalidateRect( Wnd, NULL, FALSE );
					//UpdateWindow( Wnd );

					Dialog->RefreshBtn = FALSE;
				}

				BitBlt( DC, ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right - ps.rcPaint.left, ps.rcPaint.bottom - ps.rcPaint.top, Dialog->hDCMem, ps.rcPaint.left, ps.rcPaint.top, SRCCOPY );
				EndPaint( Wnd, &ps );
			}
			else
			{
				GetClientRect( Wnd, &r );
				BitBlt( ( HDC )wParam, 0, 0, r.right, r.bottom, Dialog->hDCMem, 0, 0, SRCCOPY );
			}
			break;
		}

		case WM_DESTROY:
		{
			DeleteButton();
			DeleteImage( Dialog->PLastImg );
			DeleteWnd();

			return CallWindowProc( ( WNDPROC )GetWindowLong( Wnd, GWL_WNDPROC ), Wnd, Msg, wParam, lParam );
		}
		default:
			return CallWindowProc( Dialog->OldProc, Wnd, Msg, wParam, lParam );
	}

	return TRUE;
}

PImg *AddImage( int Ind, GpImage *Img, int l, int t, int w, int h, BOOL Stretch, BOOL IsBkg )
{
	UINT OrigWidth = 0, OrigHeight = 0;

	//PImg *cimg = new PImg;
	PImg *cimg = ( PImg * )xmalloc( sizeof( PImg ) );
	memset( cimg, 0, sizeof( PImg ) );

	cimg->Image = CreateImage( cimg->Image, Img );

	GdipGetImageWidth( cimg->Image, &OrigWidth );
	GdipGetImageHeight( cimg->Image, &OrigHeight );
	SetRect( &cimg->VisibleRect, 0, 0, OrigWidth, OrigHeight );
	cimg->Image = ConvertImageTo32bit( cimg->Image, OrigWidth, OrigHeight );

	cimg->Left = l;
	cimg->Top = t;

	if( Stretch )
	{
		cimg->Width = w;
		cimg->Height = h;
	}
	else
	{
		cimg->Width = OrigWidth;
		cimg->Height = OrigHeight;
	}

	cimg->IsBkg = IsBkg;
	cimg->Visible = TRUE;
	cimg->Stretch = Stretch;
	cimg->Transparent = 255;

	cimg->Delete = FALSE;
	cimg->WndInd = Ind;
	cimg->PNextImg = FALSE;

	if( Dialog->PFirstImg == NULL )
	{
		cimg->PPrevImg = NULL;
		Dialog->PFirstImg = cimg;
		Dialog->PLastImg = cimg;
	}
	else
	{
		Dialog->PLastImg->PNextImg = cimg;
		cimg->PPrevImg = Dialog->PLastImg;
		Dialog->PLastImg = cimg;
	}
	Dialog->RefreshBtn = FALSE;

	return cimg;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
int InitWnd( HWND hWnd )
{
	//TgdipDialog *Tmp;
	RECT rect;
	HDC DC;
	DWORD BkgColor;
	GpGraphics *pGraphics;

	Dialog = ( TgdipDialog * )xmalloc( sizeof( TgdipDialog ) );
	memset( Dialog, 0, sizeof( TgdipDialog ) );

	Dialog->Wnd = hWnd;

	Dialog->hDCMem = CreateCompatibleDC( 0 );
	GetClientRect( hWnd, &rect );

	DC = GetDC( hWnd );
	Dialog->hBmp = CreateCompatibleBitmap( DC, rect.right - rect.left, rect.bottom - rect.top );
	ReleaseDC( hWnd, DC );

	Dialog->hOld = SelectObject( Dialog->hDCMem, Dialog->hBmp );

	SetRectEmpty( &Dialog->UpdateBkgRect );
	SetRectEmpty( &Dialog->UpdateRect );

	Dialog->OldProc = ( WNDPROC )SetWindowLong( hWnd, GWL_WNDPROC, ( LONG )WndProc );


	CreateEmptyImage( &Dialog->BImg, hWnd );
	BkgColor = 0xFF000000 | GetFormColorT( Dialog );
	GdipGetImageGraphicsContext( Dialog->BImg, &pGraphics );
	GdipGraphicsClear( pGraphics, BkgColor );
	GdipDeleteGraphics( pGraphics );

	return 1;
}

int ImgLoad( GpImage *Img, int Left, int Top, int Width, int Height, BOOL Stretch, BOOL IsBkg )
{
	RECT rect = { 0 };

	PImg *img = AddImage( 0, Img, Left, Top, Width, Height, Stretch, IsBkg );
	if( img != NULL )
	{
		SetRect( &rect, Left, Top, Left + img->Width, Top + img->Height );
		if( IsBkg == TRUE )
			UnionRect( &Dialog->UpdateBkgRect, &Dialog->UpdateBkgRect, &rect );
		else
			UnionRect( &Dialog->UpdateRect, &Dialog->UpdateRect, &rect );
		
		return 1;
	}
	return 0;
}

void ImgApplyChanges( HWND wnd )
{
	RECT r;


	if( IsRectEmpty( &Dialog->UpdateBkgRect ) && IsRectEmpty( &Dialog->UpdateRect ) )
		return;

	UnionRect( &r, &Dialog->UpdateBkgRect, &Dialog->UpdateRect );

	if( !IsRectEmpty( &Dialog->UpdateRect ) )
	{
		SetFullImage( FALSE, Dialog->UpdateRect );
		SetRectEmpty( &Dialog->UpdateRect );
	}

	if( !IsRectEmpty( &Dialog->UpdateBkgRect ) )
	{
		SetFullImage( TRUE, Dialog->UpdateBkgRect );
		SetRectEmpty( &Dialog->UpdateBkgRect );
	}

	Dialog->RefreshBtn = TRUE;
	InvalidateRect( wnd, &r, FALSE );
	UpdateWindow( wnd );
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// Кнопка и иже с ней

/**
 * Проверяет, на кнопке курсор или нет
 * @param (TgdipButton) btn - Кнопка
 * @param (POINT) p - Координаты курсора
 * @return - TRUE если курсор на кнопке
 */
BOOL CursorInButton( TgdipButton *btn, POINT p )
{
	RECT rc;
	ScreenToClient( btn->Handle, &p );
	GetClientRect( btn->Handle, &rc );

	if( p.x >= rc.left && p.x <= rc.right && p.y >= rc.top && p.y <= rc.bottom )
		return TRUE;
	else
		return FALSE;
}

/**
 * Проверяет статус кнопки
 * @param (TgdipButton) btn - Кнопка
 * @param (DWORD) State - Статус для проверки
 * @return - TRUE если кнопка имеет проверяемый статус
 */
BOOL IsButtonState( TgdipButton *btn, DWORD State )
{
	return ( btn->bsState & State ) == State;
}

/**
 * Функция рисования кнопки
 * @param (HDC) hBtnDC - Дескриптор контекста отображаемого устройства
 * @param (RECT) r - координаты
 * @param (TgdipButton) btn - кнопка
 * @param (GpImage) img - Картинка для отрисовки на кнопке
 */
void DrawButton( HDC hBtnDC, RECT r, TgdipButton *btn, GpImage *img )
{
	GpGraphics *pGraphics;
	
	if( img == NULL )
		return;

	HWND hParent = GetAncestor( btn->Handle, GA_PARENT );
	SendMessage( hParent, WM_ERASEBKGND, ( WPARAM )btn->hDCMem, 0 );
	CallWindowProc( ( WNDPROC )GetWindowLong( hParent, GWL_WNDPROC ), hParent, WM_PAINT, ( WPARAM )btn->hDCMem, 0 );

	pGraphics = NULL;
	GdipCreateFromHDC( btn->hDCMem, &pGraphics );
	GdipSetSmoothingMode( pGraphics, SmoothingModeHighSpeed );
	GdipSetInterpolationMode( pGraphics, InterpolationModeHighQualityBilinear );
	GdipDrawImageRectI( pGraphics, img, btn->Left, btn->Top, btn->Width, btn->Height );
	GdipDeleteGraphics( pGraphics );

	BitBlt( hBtnDC, r.left, r.top, r.right - r.left, r.bottom - r.top, btn->hDCMem, r.left + btn->Left, r.top + btn->Top, SRCCOPY );
}

/**
 * Получаем данные кнопки из указанного хэндла
 * @param (HWND) Btn - хэндл кнопки
 */
TgdipButton *GetBtn( HWND Btn )
{
	TgdipButton *cbtn;

	if( IsWindow( Btn ) )
	{
		cbtn = ( TgdipButton * )GetWindowLong( Btn, GWL_USERDATA );
		if( cbtn != NULL )
		{
			if( cbtn->Delete == FALSE )
				return cbtn;
		}
	}
	return NULL;
}

/**
 * При завершении удаляем все данные по отрисовке кнопки/кнопок
 */
void DeleteButton( void )
{
	for( HWND wbtn : wndbtn )
	{
		TgdipButton *btn = GetBtn( wbtn );
		if( btn == NULL )
			continue;

		SetWindowLong( btn->Handle, GWL_WNDPROC, btn->OldProc );
		SetWindowLong( btn->Handle, GWL_USERDATA, 0 );
		btn->Delete = TRUE;

		if( btn->imgNormal != NULL )
		{		
			GdipDisposeImage( btn->imgNormal );
			btn->imgNormal = NULL;
		}
		
		if( btn->imgFocused != NULL )
		{
			GdipDisposeImage( btn->imgFocused );
			btn->imgFocused = NULL;
		}
		
		if( btn->imgPressed != NULL )
		{
			GdipDisposeImage( btn->imgPressed );
			btn->imgPressed = NULL;
		}
		
		if( btn->imgDisabled != NULL )
		{
			GdipDisposeImage( btn->imgDisabled );
			btn->imgDisabled = NULL;
		}

		DestroyCursor( btn->Cursor );
		xfree( btn );
	}
}

/**
 * Задаём функцию, выполнение которой будет осуществлено при нажатии на указанную кнопку
 * @param (HWND) wnd - хэндл кнопки
 * @param (*Event) - Указатель на функцию для выполнения
 */
void BtnSetEvent( HWND wnd, void( *Event )( HWND ) )
{
	TgdipButton *btn;

	btn = GetBtn( wnd );
	if( btn == NULL )
		return;

	btn->OnClick = Event;
}

static LRESULT CALLBACK BtnProc( HWND Wnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{	
	DWORD OldState;
	POINT Pt;	
	HDC hBtnDC;
	PAINTSTRUCT PBtnStr;


	btn = GetBtn( Wnd );
	if( btn == NULL )
		return NULL;

	switch( Msg )
	{
		case WM_NCPAINT:
		case WM_ERASEBKGND:
		{
			return 1;
		}

		// Курсор на кнопке
		case WM_MOUSEMOVE:
		{
			OldState = btn->bsState;
			GetCursorPos( &Pt );

			// Срабатывает когда нажзалт левую кнопку мыши и убрали курсор с кнопки
			if( CursorInButton( btn, Pt ) == FALSE )
			{
				if( btn->IsMouseLeave == FALSE )
				{
					btn->bsState = ( btn->bsState & ~bsFocused );
				}
				else
				{
					// Зажали левую кнопку мыши и убрали с кнопки
					btn->bsState = btn->bsState | bsFocused;
				}
			}
						
			if( CursorInButton( btn, Pt ) == TRUE )
			{
				if( btn->bsNextBtnTrack == FALSE )
				{
					TRACKMOUSEEVENT tme = { 0 };

					tme.cbSize = sizeof( TRACKMOUSEEVENT );
					tme.hwndTrack = Wnd;
					tme.dwFlags = TME_LEAVE;
					tme.dwHoverTime = HOVER_DEFAULT;
					btn->bsNextBtnTrack = TrackMouseEvent( &tme );
					btn->IsMouseLeave = FALSE;
										
					///////////
					//if( btn->OnMouseEnter != NULL )
					//	btn->OnMouseEnter = Wnd;
					//BtnEvent( btn->OnMouseEnter )( Wnd )
				}

				btn->bsState = btn->bsState | bsFocused;

				/*if( !btn->IsMouseLeave )
				{
					if( !btn->OnMouseMove )
						btn->OnMouseMove = Wnd;
					//TBtnEventProc( btn->OnMouseMove )( Wnd );

				}*/
			}
			else
			{
				if( btn->IsMouseLeave == FALSE )
				{
					if( IsButtonState( btn, bsPressed ) == FALSE )
					{
						btn->IsMouseLeave = TRUE;
						btn->bsNextBtnTrack = FALSE;
					}
				}
			}

			if( btn->bsState != OldState )
			{
				InvalidateRect( Wnd, NULL, FALSE );
				UpdateWindow( Wnd );
			}

			break;
		}

		// Убрали курсор с кнопки
		case WM_MOUSELEAVE:
		{
			if( btn->IsMouseLeave == FALSE )
			{
				OldState = btn->bsState;
				btn->IsMouseLeave = TRUE;
				btn->bsState = ( btn->bsState & ~bsFocused );
				btn->bsNextBtnTrack = FALSE;
				if( btn->bsState != OldState )
				{
					InvalidateRect( Wnd, NULL, FALSE );
					UpdateWindow( Wnd );
				}
				//if( !btn->OnMouseLeave != NULL )
				//	btn->OnMouseLeave = Wnd;
			}
			break;
		}

		//Нажали левую кнопку мышки
		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK:
		{
			if( IsButtonState( btn, bsEnabled ) )
			{
				GetCursorPos( &Pt );
				if( CursorInButton( btn, Pt ) == TRUE )
				{
					if( GetCapture() != Wnd )
						SetCapture( Wnd );

					OldState = btn->bsState;

					btn->bsState = btn->bsState | bsFocused | bsPressed;
					if( btn->bsState != OldState )
					{
						InvalidateRect( Wnd, NULL, FALSE );
						UpdateWindow( Wnd );
					}
					//if( btn->OnMouseDown != NULL )
					//{
					//	btn->OnMouseDown = Wnd;
					//}
				}
				else
				{
					return SendMessage( GetAncestor( Wnd, GA_PARENT ), Msg, wParam, lParam );
				}
			}

			break;
		}
		
		case WM_LBUTTONUP:
		{
			if( IsButtonState( btn, bsEnabled ) )
			{
				ReleaseCapture();

				if( IsButtonState( btn, bsEnabled ) )
				{
					OldState = btn->bsState;
					btn->bsState = btn->bsState & ~bsPressed;
					GetCursorPos( &Pt );

					if( CursorInButton( btn, Pt ) )
					{
						if( IsButtonState( btn, bsChecked ) )
							btn->bsState = btn->bsState & ~bsChecked;
						else
							btn->bsState = btn->bsState | bsChecked;
					}
					if( btn->bsState != OldState )
					{
						InvalidateRect( Wnd, NULL, FALSE );
						UpdateWindow( Wnd );
					}

					if( CursorInButton( btn, Pt ) && btn->OnClick != NULL )
						btn->OnClick( Wnd );
				}
			}
			break;
		}

		case WM_ENABLE:
		{
			OldState = btn->bsState;

			if( ( BOOL )wParam )
				btn->bsState = btn->bsState | bsEnabled;
			else
				btn->bsState = 0;

			if( btn->bsState != OldState )
			{
				InvalidateRect( Wnd, NULL, FALSE );
				UpdateWindow( Wnd );
			}
			return CallWindowProc( ( WNDPROC )btn->OldProc, Wnd, Msg, wParam, lParam );
		}

		case WM_SETCURSOR:
		{
			GetCursorPos( &Pt );

			if( !CursorInButton( btn, Pt ) )
				SetCursor( btn->Cursor );
			else
				return SendMessage( GetAncestor( Wnd, GA_PARENT ), WM_SETCURSOR, ( WPARAM )GetAncestor( Wnd, GA_PARENT ), lParam );

			break;
		}

		case WM_WINDOWPOSCHANGING:
		{
			if( ( PWINDOWPOS( lParam )->flags && SWP_NOSIZE == 0 ) || ( PWINDOWPOS( lParam )->flags && SWP_NOMOVE == 0 ) )
			{
				if( PWINDOWPOS( lParam )->flags && SWP_NOSIZE == 0 )
				{
					btn->Width = PWINDOWPOS( lParam )->cx;
					btn->Height = PWINDOWPOS( lParam )->cy;
				}
				if( PWINDOWPOS( lParam )->flags && SWP_NOMOVE == 0 )
				{
					btn->Left = PWINDOWPOS( lParam )->x;
					btn->Top = PWINDOWPOS( lParam )->y;
				}

				PWINDOWPOS( lParam )->flags = PWINDOWPOS( lParam )->flags | SWP_NOCOPYBITS;
			}
			break;
		}

		case WM_PAINT:
		{
			hBtnDC = BeginPaint( Wnd, &PBtnStr );

			if( IsButtonState( btn, bsEnabled ) )
			{
				if( IsButtonState( btn, bsPressed ) && IsButtonState( btn, bsFocused ) )
				{
					DrawButton( hBtnDC, PBtnStr.rcPaint, btn, btn->imgPressed );
				}
				else
				{
					if( !IsButtonState( btn, bsPressed ) && IsButtonState( btn, bsFocused ) )
					{
						DrawButton( hBtnDC, PBtnStr.rcPaint, btn, btn->imgFocused );
					}
					else
					{
						DrawButton( hBtnDC, PBtnStr.rcPaint, btn, btn->imgNormal );
					}
				}
			}
			else
			{
				DrawButton( hBtnDC, PBtnStr.rcPaint, btn, btn->imgDisabled );
			}

			EndPaint( Wnd, &PBtnStr );
			break;
		}

		case WM_DESTROY:
		{
			return CallWindowProc( ( WNDPROC )GetWindowLong( Wnd, GWL_WNDPROC ), Wnd, Msg, wParam, lParam );
		}

		default:
			break;
	}

	return CallWindowProc( ( WNDPROC )btn->OldProc, Wnd, Msg, wParam, lParam );
}

/**
 * Создание кнопки на форме
 * @param (HWND) hWnd - Хэндл окна где будет отрисована кнопка
 * @param (GpImage) BtnImg - изображение кнопки
 * @param (int) Left/Top/Width/Height - координаты расположения кнопки на окне
 * @return (HWND) - Возвращает хэндл созданной кнопки, или NULL в случае неудачи
 */
HWND BtnCreate( HWND hWnd, GpImage *BtnImg, int Left, int Top, int Width, int Height )
{
	GpBitmap *img = NULL, *simg;
	GpGraphics *g1, *g;
	UINT ImgCount, iw, ih;
	RECT rect;
	HDC DC;
	TgdipButton *btn;

	//int TT = sizeof( TgdipButton );

	btn = ( TgdipButton * )xmalloc( sizeof( TgdipButton ) );
	if( btn == NULL )
		return NULL;

	HWND Result = CreateWindow( L"BUTTON", L"", WS_CHILD | WS_CLIPSIBLINGS | WS_TABSTOP | BS_OWNERDRAW | WS_VISIBLE, Left, Top, Width, Height, hWnd, NULL, /*hInstance*/( HINSTANCE )GetWindowLong( hWnd, GWL_HINSTANCE ), NULL );

	if( Result == NULL )
		return NULL;
		
	btn->Handle = Result;
	btn->bsState = bsEnabled;
	btn->bsNextBtnTrack = FALSE;

	img = CreateImage( img, BtnImg );

	GdipGetImageWidth( img, &iw );
	GdipGetImageHeight( img, &ih );

	ImgCount = 4;

	ih = ih / ImgCount;

	GdipGetImageGraphicsContext( img, &g1 );

	for( UINT i = 0; i < ImgCount; ++i )
	{
		GdipCreateBitmapFromGraphics( iw, ih, g1, &simg );
		GdipGetImageGraphicsContext( simg, &g );
		GdipSetSmoothingMode( g, SmoothingModeAntiAlias );
		GdipSetInterpolationMode( g, InterpolationModeHighQualityBicubic );
		GdipDrawImageRectRectI( g, img, 0, 0, iw, ih, 0, ih * i, iw, ih, UnitPixel, NULL, NULL, NULL );
		GdipDeleteGraphics( g );

		switch( i )
		{
			case 0: btn->imgNormal = simg; break;
			case 1: btn->imgFocused = simg; break;
			case 2: btn->imgPressed = simg; break;
			case 3: btn->imgDisabled = simg; break;
		}
	}

	GdipDisposeImage( img );
	GdipDeleteGraphics( g1 );

	btn->hDCMem = CreateCompatibleDC( 0 );
	GetClientRect( hWnd, &rect );
	DC = GetDC( hWnd );
	btn->hBmp = CreateCompatibleBitmap( DC, rect.right, rect.bottom );
	ReleaseDC( hWnd, DC );
	btn->hOld = SelectObject( btn->hDCMem, btn->hBmp );

	btn->Left = Left;
	btn->Top = Top;
	btn->Width = Width;
	btn->Height = Height;

// 	btn.OnClick = NULL;
// 	btn.OnMouseEnter = NULL;
// 	btn.OnMouseLeave = NULL;
// 	btn.OnMouseMove = NULL;
// 	btn.OnMouseDown = NULL;
// 	btn.OnMouseUp = NULL;
	btn->IsMouseLeave = TRUE;

	btn->OnClick = NULL;

	btn->Cursor = LoadCursor( 0, L"OCR_NORMAL" );

	btn->Delete = FALSE;
	btn->Visible = TRUE;

	btn->OldProc = SetWindowLong( Result, GWL_WNDPROC, ( LONG )BtnProc );
	SetWindowLong( Result, GWL_USERDATA, ( LONG )btn );

	InvalidateRect( Result, NULL, FALSE );

	wndbtn.emplace_back( Result );

	return Result;
}