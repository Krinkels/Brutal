/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
#include <windows.h>
#include "Botva.h"
#include "MemoryModule.h"
#include "resource.h"

HMEMORYMODULE brutal_dll;

GDIConstructor_f GDIConstructorb;
ImgLoad_f ImgLoad;
BtnCreate_f BtnCreate;
BtnSetEvent_f BtnSetEvent;
ImgApplyChanges_f ImgApplyChanges;
gdipShutdown_f gdipShutdown;
CreateFormFromImage_f CreateFormFromImag;
BtnSetVisibility_f BtnSetVisibility;
ImgSetTransparent_f ImgSetTransparent;
ImgRelease_f ImgRelease;

bool BotvaInit( void )
{
	brutal_dll = LoadDll( IDR_BRUTAL );
	if( !brutal_dll )
		return false;

	GDIConstructorb = ( GDIConstructor_f )MemoryGetProcAddress( brutal_dll, "id_000000000" );
	ImgLoad = ( ImgLoad_f )MemoryGetProcAddress( brutal_dll, "id_111000000" );
	BtnCreate = ( BtnCreate_f )MemoryGetProcAddress( brutal_dll, "id_110000000" );
	BtnSetEvent = ( BtnSetEvent_f )MemoryGetProcAddress( brutal_dll, "id_110010011" );
	ImgApplyChanges = ( ImgApplyChanges_f )MemoryGetProcAddress( brutal_dll, "id_111000010" );
	CreateFormFromImag = ( CreateFormFromImage_f )MemoryGetProcAddress( brutal_dll, "id_001000000" );
	BtnSetVisibility = ( BtnSetVisibility_f )MemoryGetProcAddress( brutal_dll, "id_110111110" );
	ImgRelease = ( ImgRelease_f )MemoryGetProcAddress( brutal_dll, "id_111000110" );
	ImgSetTransparent = ( ImgSetTransparent_f )MemoryGetProcAddress( brutal_dll, "id_111011110" );
	gdipShutdown = ( gdipShutdown_f )MemoryGetProcAddress( brutal_dll, "id_000000001" );

	if( !GDIConstructorb || !ImgLoad || !BtnCreate || !BtnSetEvent || !ImgApplyChanges || !gdipShutdown || !CreateFormFromImag || 
		!BtnSetVisibility || !ImgRelease || !ImgSetTransparent )
		return false;

	GDIConstructorb();


	return true;
}

void BotvaDeInit( void )
{
	gdipShutdown();
	MemoryFreeLibrary( brutal_dll );
}