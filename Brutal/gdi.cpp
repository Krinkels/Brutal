/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
#include <windows.h>
#include "gdi.h"

#pragma comment(lib,"gdiplus.lib")

BOOL IsGDIInit = FALSE;
ULONG_PTR GDIToken = NULL;

void GDIConstructor( void )
{
	GdiplusStartupInput GDIInp;

	if( IsGDIInit )
		return;

	memset( &GDIInp, 0, sizeof( GdiplusStartupInput ) );
	GDIInp.GdiplusVersion = 1;
	GdiplusStartup( &GDIToken, &GDIInp, NULL );
	IsGDIInit = TRUE;
}

void GDIDestructor( void )
{
	if( IsGDIInit )
	{
		GdiplusShutdown( GDIToken );
		IsGDIInit = FALSE;
	}
}

BOOL ImageFromResource( UINT ID, GpImage **image )
{
	if( !IsGDIInit )
		return FALSE;
	// ���� ������ PNG ������� � ������� �������� 
	HRSRC ResourceHandle = FindResource( NULL, MAKEINTRESOURCE( ID ), L"PNG" );
	if( ResourceHandle )
	{
		DWORD Size = SizeofResource( NULL, ResourceHandle );
		if( Size )
		{
			HGLOBAL Resource = LoadResource( NULL, ResourceHandle );
			if( Resource )
			{
				const void* vData = LockResource( Resource );
				HGLOBAL hData = GlobalAlloc( GMEM_MOVEABLE, Size );
				IStream *pIStream = NULL;
				CopyMemory( GlobalLock( hData ), vData, Size );
				if( CreateStreamOnHGlobal( hData, TRUE, &pIStream ) == S_OK )
				{
					//( *image ) = Gdiplus::Image::FromStream( pIStream );
					GdipLoadImageFromStream( pIStream, image );
					pIStream->Release();
				}
				GlobalUnlock( hData );
				GlobalFree( hData );
				return TRUE;
			}
		}
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////

void CreateEmptyImage( GpBitmap **img, HWND hWnd )
{
	RECT cRect;

	GetClientRect( hWnd, &cRect );

	GdipCreateBitmapFromScan0( cRect.right, cRect.bottom, 0, PixelFormat32bppPARGB, NULL, img );
}

DWORD GetFormColorT( TgdipDialog *Tmp )
{
	HDC MemDC;
	HBITMAP BMP;
	HGDIOBJ oldBMP;
	DWORD Clr;
	DWORD r, g, b;
	int RedShift = 16;
	int GreenShift = 8;
	int BlueShift = 0;


	MemDC = CreateCompatibleDC( Tmp->hDCMem );
	BMP = CreateCompatibleBitmap( Tmp->hDCMem, 1, 1 );
	oldBMP = SelectObject( MemDC, BMP );
	CallWindowProc( Tmp->OldProc, Tmp->Wnd, WM_ERASEBKGND, ( WPARAM )MemDC, 0 );

	Clr = GetPixel( MemDC, 0, 0 );
	SelectObject( MemDC, oldBMP );
	DeleteDC( MemDC );
	DeleteObject( BMP );

	b = Clr >> RedShift;
	Clr = Clr - ( b << RedShift );
	g = Clr >> GreenShift;
	Clr = Clr - ( g << GreenShift );
	r = Clr >> BlueShift;

	return ( DWORD( b ) << BlueShift ) | ( DWORD( g ) << GreenShift ) | ( DWORD( r ) << RedShift );
}

GpBitmap *CreateImage( GpBitmap *img, GpImage *CloneImg )
{
	//GpImage *CloneImg = NULL;
	GpGraphics *g, *g1;
	UINT iw, ih;

	if( img )
	{
		GdipDisposeImage( img );
		img = NULL;
	}

	GdipGetImageWidth( CloneImg, &iw );
	GdipGetImageHeight( CloneImg, &ih );

	g1 = NULL;
	GdipGetImageGraphicsContext( CloneImg, &g1 );

	img = NULL;
	GdipCreateBitmapFromGraphics( iw, ih, g1, &img );

	g = NULL;
	GdipGetImageGraphicsContext( img, &g );
	GdipDrawImageRectI( g, CloneImg, 0, 0, iw, ih );
	GdipDeleteGraphics( g );
	GdipDisposeImage( CloneImg );
	GdipDeleteGraphics( g1 );

	return img;
}

GpBitmap *ConvertImageTo32bit( GpBitmap *img, int Width, int Height )
{
	GpBitmap *CloneImg;

	GdipCloneBitmapAreaI( 0, 0, Width, Height, PixelFormat32bppPARGB, img, &CloneImg );
	GdipDisposeImage( img );
	img = CloneImg;
	return img;
}