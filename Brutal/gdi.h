#pragma once

#include <windows.h>
#include <gdiplus.h>
#include <GdiplusGpStubs.h>

using namespace Gdiplus;
using namespace Gdiplus::DllExports;

typedef struct  m_PImg
{
	GpBitmap *Image;
	RECT VisibleRect;
	int Left;
	int Top;
	int Width;
	int Height;
	BOOL IsBkg;
	BOOL Visible;
	BOOL Stretch;
	Gdiplus::REAL Transparent;
	BOOL Delete;
	int WndInd;
	struct m_PImg *PNextImg;
	struct m_PImg *PPrevImg;
}PImg;

typedef struct
{
	HWND Wnd;
	HDC hDCMem;
	HBITMAP hBmp;
	HGDIOBJ hOld;
	RECT UpdateBkgRect;
	RECT UpdateRect;
	WNDPROC OldProc;
	GpBitmap *BImg;
	GpBitmap *FImg;
	BOOL RefreshBtn;

	PImg *PFirstImg;
	PImg *PLastImg;
}TgdipDialog;

typedef struct
{
	HWND Handle;
	int Left;
	int Top;
	int Width;
	int Height;
	LONG OldProc;
	DWORD bsState;
	BOOL bsNextBtnTrack;
	GpBitmap *imgNormal;
	GpBitmap *imgFocused;
	GpBitmap *imgPressed;
	GpBitmap *imgDisabled;

	BOOL IsMouseLeave;

	void( *OnClick )( HWND );

	BOOL Delete;
	BOOL Visible;

	HDC hDCMem;
	HBITMAP hBmp;
	HGDIOBJ hOld;
	HCURSOR Cursor;
}TgdipButton;

void GDIConstructor();
void GDIDestructor();
BOOL ImageFromResource( UINT ID, GpImage **image );
//////////////////////////////////////////////////////////////////////////
void CreateEmptyImage( GpBitmap **img, HWND hWnd );
DWORD GetFormColorT( TgdipDialog *Tmp );
GpBitmap *CreateImage( GpBitmap *img, GpImage *CloneImg );
GpBitmap *ConvertImageTo32bit( GpBitmap *img, int Width, int Height );