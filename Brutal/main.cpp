/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
#include <windows.h>
#include <stdio.h>
#include "gdi.h"
#include "images.h"
#include "xmalloc.h"
#include "resource.h"

#define SNAPTOEDGESENSIVITY 25
HWND GlobalWnd = NULL;

/**
 * ����� ������ ���� + ���������� ��� �� �������� �������
 * @param (HWND) hWnd - ����� ������ ����
 */
void Init( HWND hWnd )
{
	// ������ ����
#define iw 693
#define ih 427

	HDC hDCScreen = GetDC( NULL );
	int nHorRes = GetDeviceCaps( hDCScreen, HORZRES );
	int nVerRes = GetDeviceCaps( hDCScreen, VERTRES );
	ReleaseDC( NULL, hDCScreen );
	int x1 = nHorRes / 2 - iw / 2;
	int y1 = nVerRes / 2 - ih / 2;
	SetWindowPos( hWnd, 0, x1, y1, iw, ih, SWP_NOZORDER | SWP_NOACTIVATE );
}

/**
 * ������ ������� ����
 * @param (HWND) wnd - ����� ������, ������� ����� �������� �� BtnProc
 */
void mRunGame( HWND wnd )
{
	MessageBox( 0, L"Test", NULL, MB_OK );
}

//////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK MainDlgProc( HWND hWndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	UNREFERENCED_PARAMETER( lParam );
	UNREFERENCED_PARAMETER( wParam );

	switch( uMsg )
	{
		case WM_INITDIALOG:
		{
			GlobalWnd = hWndDlg;

			// ������� �����
			SetWindowLongPtr( hWndDlg, GWL_STYLE, 0 );
			SetWindowLongPtr( hWndDlg, GWL_EXSTYLE, 0 );

			Init( hWndDlg );
						
			//////////////////////////////////////////////////////////////////////////
			// ���������� �����
			RECT r;
			int w = 15;

			GetWindowRect( hWndDlg, &r );

			int h = ( r.right - r.left ) > ( w * 2 ) ? w : ( r.right - r.left );
			int v = ( r.bottom - r.top ) > ( w * 2 ) ? w : ( r.bottom - r.top );

			//h = ( h < v ) ? h : v;
			int z = min( h, v );
			HRGN hRgn1 = CreateRoundRectRgn( 0, 0, ( r.right - r.left ), ( r.bottom - r.top ), z, z );
			CombineRgn( hRgn1, 0, hRgn1, RGN_DIFF );
			SetWindowRgn( hWndDlg, hRgn1, 1 );
			//////////////////////////////////////////////////////////////////////////

			InitDebugMemory();
			GDIConstructor();
			InitWnd( hWndDlg );

			GpImage *Img = nullptr;
			ImageFromResource( IDB_FON, &Img );
			if( !Img )
				exit( 0 );

			ImgLoad( Img, 0, 0, 0, 0, FALSE, TRUE );

			//////////////////////////////////////////////////////////////////////////
			// ������ ��������� 
			ImageFromResource( IDB_RUN, &Img );
			if( !Img ) return 0;
			HWND hRun = BtnCreate( hWndDlg, Img, 286, 370, 130, 50);
			BtnSetEvent( hRun, mRunGame );

			// ������ ��������
			ImageFromResource( IDB_MIN, &Img );
			if( !Img ) return 0;
			//HWND BTN_Min = BtnCreate( hWndDlg, 635, 15, 20, 20, Img, 0, FALSE );
			HWND BTN_Min = BtnCreate( hWndDlg, Img, r.right - r.left - 58, 15, 20, 20 );
			BtnSetEvent( BTN_Min, []( HWND wnd )
			{
				ShowWindow( GlobalWnd, SW_MINIMIZE );
			} );

			//int x = ( r.right - r.left ) / 2;	// 693
			//int y = r.bottom - r.top;	// 427

			// ������ �������						
			ImageFromResource( IDB_CLOSE, &Img );
			if( !Img ) return 0;
			HWND BTN_Close = BtnCreate( hWndDlg, Img, r.right - r.left - 33, 15, 20, 20 );
			/*BtnSetEvent( BTN_Close, 1, []()
			{
				mSave();
				PostQuitMessage( 0 );
			} );*/

			/*ImageFromResource( IDB_RUN, &Img );
			if( !Img ) 
				return 0;
			BtnCreate( hWndDlg, Img, 10, 10, 100, 100 );

			ImageFromResource( IDB_SAVE, &Img );
			if( !Img )
				return 0;
			BtnCreate( hWndDlg, Img, 150, 150, 100, 100 );*/

			ImgApplyChanges( hWndDlg );

			break;
		}

		case WM_LBUTTONDOWN:
		{
			// �������������� ���� ����� �� ����� �������
			DefWindowProc( hWndDlg, uMsg, wParam, lParam );
			PostMessage( hWndDlg, WM_SYSCOMMAND, 0xf012, 0 );
			break;
		}

		case WM_WINDOWPOSCHANGING:
		{
			// "����������" ���� � ����� ��������
			WINDOWPOS *lpWindowPos = ( WINDOWPOS* )lParam;

			HMONITOR curMonitor = MonitorFromWindow( hWndDlg, MONITOR_DEFAULTTONEAREST );
			MONITORINFO monInfo;
			monInfo.cbSize = sizeof( monInfo );
			GetMonitorInfo( curMonitor, &monInfo );

			RECT *dr = &monInfo.rcWork;

			// Left side
			if( lpWindowPos->x < dr->left + SNAPTOEDGESENSIVITY && lpWindowPos->x > dr->left - SNAPTOEDGESENSIVITY )
				lpWindowPos->x = dr->left;

			// Right side
			if( dr->right - lpWindowPos->x - lpWindowPos->cx  < SNAPTOEDGESENSIVITY && dr->right - lpWindowPos->x - lpWindowPos->cx > -SNAPTOEDGESENSIVITY )
				lpWindowPos->x = dr->right - lpWindowPos->cx;

			// Top side
			if( lpWindowPos->y < dr->top + SNAPTOEDGESENSIVITY && lpWindowPos->y > dr->top - SNAPTOEDGESENSIVITY )
				lpWindowPos->y = dr->top;

			// Bottom side
			if( dr->bottom - lpWindowPos->y - lpWindowPos->cy  < SNAPTOEDGESENSIVITY && dr->bottom - lpWindowPos->y - lpWindowPos->cy > -SNAPTOEDGESENSIVITY )
				lpWindowPos->y = dr->bottom - lpWindowPos->cy;

			break;
		}
				
		case WM_DESTROY:
		{
			GDIDestructor();

			print_malloc_debug_stats();

			PostQuitMessage( 0 );
			break;
		}

		case WM_CLOSE:
		{
			/*spShutdown();
			FreeLibrary( hinstLib );*/
						
			//DestroyWindow( hWndDlg );
			EndDialog( hWndDlg, 0 );
			break;
		}
		default: 
			break;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
class APP_EXISTER
{
	HANDLE event;
	bool exists;

public:
	APP_EXISTER( const wchar_t *name )
	{
		event = OpenEvent( EVENT_MODIFY_STATE, false, name );
		exists = ( event != INVALID_HANDLE_VALUE ) && ( event != NULL );
		if( !exists ) event = CreateEvent( NULL, true, false, name );
	}

	~APP_EXISTER()
	{
		if( ( event != INVALID_HANDLE_VALUE ) && ( event != NULL ) ) CloseHandle( event );
	}

	bool is_exists()
	{
		return exists;
	}
};

INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	UNREFERENCED_PARAMETER( nCmdShow );
	UNREFERENCED_PARAMETER( lpCmdLine );

	APP_EXISTER app_exst( L"BRUTAL_EVENT" );

	if( app_exst.is_exists() )
	{
		//		MessageBox( 0, L"������ ����� ��� ����������, ��������� ����������", L"", 0 );
		return 0;
	}

	return DialogBoxParamW( hInstance, MAKEINTRESOURCE( MAIN_WINDOW ), NULL, MainDlgProc, ( LPARAM )hInstance ); //false;
}