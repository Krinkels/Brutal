#pragma once
#include "gdi.h"

typedef struct
{
	Gdiplus::REAL Left;
	Gdiplus::REAL Top;
	Gdiplus::REAL Right;
	Gdiplus::REAL Bottom;
}TRectF;

int InitWnd( HWND hWnd );
int ImgLoad( GpImage *Img, int Left, int Top, int Width, int Height, BOOL Stretch, BOOL IsBkg );
HWND BtnCreate( HWND hWnd, GpImage *BtnImg, int Left, int Top, int Width, int Height );
void ImgApplyChanges( HWND wnd );
void BtnSetEvent( HWND wnd, void( *Event )( HWND ) );