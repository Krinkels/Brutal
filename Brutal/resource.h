//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Resource.rc
//
#define IDYES                           6
#define IDNO                            7
#define IDC_SAVEREPORT                  8
#define IDB_FON                         105
#define IDB_PNG1                        109
#define IDB_CLOSE                       110
#define IDB_PNG3                        111
#define IDB_MIN                         111
#define IDR_DLL1                        112
#define IDR_BRUTAL1                     114
#define IDR_BRUTAL                      114
#define IDB_PNG2                        115
#define IDB_BDS                         115
#define IDB_SETTING                     116
#define IDB_PNG5                        117
#define IDB_SAVE                        117
#define IDR_FONT                        118
#define IDI_ICON_DOOM                   119
#define IDR_MANIFEST1                   120
#define IDB_PNG4                        121
#define IDB_RUN                         121
#define IDD_CRASHDIALOG                 122
#define IDD_CRASHDETAILS                133
#define MAIN_WINDOW                     134
#define IDB_WWW                         136
#define IDD_DIALOG1                     137
#define MAIN_SELECT_A                   137
#define IDD_CREATE_ENTRY_MOD            139
#define IDD_CRASHOVERVIEW               147
#define IDC_COMBO1                      1001
#define IDC_HUD                         1001
#define IDC_COMBO_WAD                   1001
#define IDC_EDIT1                       1003
#define IDC_NAME_PAK                    1003
#define IDC_RICHEDIT22                  1005
#define IDC_MOD                         1006
#define IDC_COMBO4                      1009
#define IDC_MAP                         1009
#define IDC_CRASHINFO                   1011
#define IDC_LIST1                       1012
#define IDC_LIST_OTHER_F                1012
#define IDC_BUTTON1                     1013
#define IDC_ADDNOD                      1013
#define IDC_ADDMOD                      1013
#define IDC_ADD_MOD                     1013
#define IDC_ADD_FILE                    1013
#define IDC_SAVE                        1013
#define IDC_TREE1                       1014
#define IDC_MOD_TREE                    1014
#define IDC_LIST2                       1015
#define IDC_LIST_MOD                    1015
#define IDC_PROGRESS1                   1016
#define IDC_PROGRESS_COPY               1016
#define IDC_CRASHFILECONTENTS           1049
#define IDC_CRASHFILES                  1054
#define IDC_CRASHHEADER                 1059
#define IDC_PLEASETELLUS                1061
#define IDC_DEADGUYVIEWER               1063
#define IDC_CRASHFILESIZE               1066
#define IDC_CRASHTAB                    1074
#define IDC_CRASHSUMMARY                1075
#define IDC_COMBO_MOD                   10001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
