/*
* This is an open source non-commercial project. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include "xmalloc.h"

#define PTR_FORMAT(p) (int) (2 * sizeof (void *)), (unsigned long) (p)

int AllocMem = 0;

static void memfatal (const char *context, long attempted_size)
{
	char Buf[ 256 ];
	sprintf( Buf, "%s: Failed to allocate %ld bytes; memory exhausted.\n", context, attempted_size );
	OutputDebugStringA( Buf );
	exit (1);
}

void DebugConsolePrint( const char * format, ... )
{
	char buffer[ 512 ];
	va_list args;
	va_start( args, format );
	vsprintf( buffer, format, args );
	OutputDebugStringA( buffer );
	va_end( args );
}

#ifdef DEBUG_MALLOC
# define STATIC_IF_DEBUG static
#else
# define STATIC_IF_DEBUG
#endif

STATIC_IF_DEBUG void *
checking_malloc (size_t size)
{
	void *ptr = malloc (size);
	if (!ptr)
		memfatal ("malloc", size);

	AllocMem += size;

	return ptr;
}

STATIC_IF_DEBUG void *
checking_malloc0 (size_t size)
{
	void *ptr = calloc (1, size);
	if (!ptr)
		memfatal ("calloc", size);

	AllocMem += size;

	return ptr;
}

STATIC_IF_DEBUG void *
checking_realloc (void *ptr, size_t newsize)
{
	void *newptr;

	if (ptr)
		newptr = realloc (ptr, newsize);
	else
		newptr = malloc (newsize);
	if (!newptr)
		memfatal ("realloc", newsize);
	return newptr;
}

STATIC_IF_DEBUG char *
checking_strdup (const char *s)
{
	char *copy;

#ifndef HAVE_STRDUP
	int l = strlen (s);
	copy = ( char * )malloc (l + 1);
	if (!copy)
		memfatal ("strdup", l + 1);
	memcpy (copy, s, l + 1);
#else  /* HAVE_STRDUP */
	copy = strdup (s);
	if (!copy)
		memfatal ("strdup", 1 + strlen (s));
#endif /* HAVE_STRDUP */

	return copy;
}

STATIC_IF_DEBUG void
checking_free (void *ptr)
{
	assert (ptr != NULL);

	free (ptr);
}

#ifdef DEBUG_MALLOC

static int malloc_count, free_count;
//static mutex_t	mutexMallocTable, mutexCounter;

//#define SZ 100003			   /* Prime just over 100,000.  */
#define SZ 10000000				// 10 million

static struct
{
	const void	*ptr;
	const char	*file;
	int			line;
} malloc_table[SZ];

void
InitDebugMemory( void )
{
	memset( malloc_table, 0, SZ * sizeof( malloc_table[0] ) );	
}

void
DestroyDebugMemory( void )
{
	//OutputDebugStringA( "--------------------------------End---------------------------------------\n" );
}

unsigned long Hash_Pointer( const void *ptr )
{
	uintptr_t key = ( uintptr_t )ptr;
	key += ( key << 12 );
	key ^= ( key >> 22 );
	key += ( key << 4 );
	key ^= ( key >> 9 );
	key += ( key << 10 );
	key ^= ( key >> 2 );
	key += ( key << 7 );
	key ^= ( key >> 12 );
#if SIZEOF_VOID_P > 4
	key += ( key << 44 );
	key ^= ( key >> 54 );
	key += ( key << 36 );
	key ^= ( key >> 41 );
	key += ( key << 42 );
	key ^= ( key >> 34 );
	key += ( key << 39 );
	key ^= ( key >> 44 );
#endif
	return ( unsigned long )key;
}

static __inline int
ptr_position (const void *ptr)
{
	int i = Hash_Pointer (ptr) % SZ;
	for (; malloc_table[i].ptr != NULL; i = (i + 1) % SZ)
		if (malloc_table[i].ptr == ptr)
			return i;
	return i;
}

/* Register PTR in malloc_table.  Abort if this is not possible
   (presumably due to the number of current allocations exceeding the
   size of malloc_table.)  */

static __inline void
register_ptr (const void *ptr, const char *file, int line)
{
	int i;

	if (malloc_count - free_count > SZ)
	{
		DebugConsolePrint( "Increase SZ to a larger value and recompile. (%s)\n", __FILE__ );
		abort ();
	}

	i = ptr_position (ptr);
	malloc_table[i].ptr = ptr;
	malloc_table[i].file = file;
	malloc_table[i].line = line;
}

/* Unregister PTR from malloc_table.  Return false if PTR is not
   present in malloc_table.  */

static __inline bool
unregister_ptr (void *ptr)
{
	int i = ptr_position( ptr );
	if (malloc_table[i].ptr == NULL)
	{
		return false;
	}
	malloc_table[i].ptr = NULL;

	/* Relocate malloc_table entries immediately following PTR. */
	for (i = (i + 1) % SZ; malloc_table[i].ptr != NULL; i = (i + 1) % SZ)
	{
		const void *ptr2 = malloc_table[i].ptr;
		/* Find the new location for the key. */
		int j = Hash_Pointer (ptr2) % SZ;
		for (; malloc_table[j].ptr != NULL; j = (j + 1) % SZ)
			if (ptr2 == malloc_table[j].ptr)
				/* No need to relocate entry at [i]; it's already at or near
				   its hash position. */
				goto cont_outer;
		malloc_table[j] = malloc_table[i];
		malloc_table[i].ptr = NULL;
cont_outer:
		;
	}
	return true;
}

/* Print the malloc debug stats gathered from the above information.
   Currently this is the count of mallocs, frees, the difference
   between the two, and the dump of the contents of malloc_table.  The
   last part are the memory leaks.  */

void
print_malloc_debug_stats (void)
{
	int		i;
	bool 	printed = false;

	OutputDebugStringA( "--------------------------------Start-------------------------------------\n" );

	DebugConsolePrint( "\nPrinting malloc debug stats:\n" );
	DebugConsolePrint( "Malloc : %d\n", malloc_count );
	DebugConsolePrint( "Free   : %d\n", free_count );
	DebugConsolePrint( "Balance: %d\n", malloc_count - free_count );
	DebugConsolePrint( "Memory alloc: %d bt\n\n", AllocMem );

	// Check if we need to print anything
	for (i = 0; i < SZ; i++)
		if (malloc_table[i].ptr != NULL)
		{
			printed = true;
			break;
		}

	if ( printed )
	{
		DebugConsolePrint( "  Pointer\t\tSource filename:Line\n"
				"-----------------------------------------------------------\n" );
		for (i = 0; i < SZ; i++)
			if (malloc_table[i].ptr != NULL)
				DebugConsolePrint( "0x%0*lx: %s:%d\n", PTR_FORMAT( malloc_table[i].ptr ),
						malloc_table[i].file, malloc_table[i].line );
		DebugConsolePrint( "-----------------------------------------------------------\n" );
	}
	else
	{
		DebugConsolePrint( "All memory was successfully freed\n" );
		OutputDebugStringA( "--------------------------------End---------------------------------------\n" );
	}
}

void *
debugging_malloc (size_t size, const char *source_file, int source_line)
{
	void *ptr = checking_malloc (size);
	++malloc_count;
	register_ptr (ptr, source_file, source_line);
	return ptr;
}

void *
debugging_malloc0 (size_t size, const char *source_file, int source_line)
{
	void *ptr = checking_malloc0 (size);
	++malloc_count;
	register_ptr (ptr, source_file, source_line);
	return ptr;
}

void *
debugging_realloc (void *ptr, size_t newsize, const char *source_file, int source_line)
{
	void *newptr = checking_realloc (ptr, newsize);
	if (!ptr)
	{
		++malloc_count;
		register_ptr (newptr, source_file, source_line);
	}
	else if (newptr != ptr)
	{
		unregister_ptr (ptr);
		register_ptr (newptr, source_file, source_line);
	}
	return newptr;
}

char *
debugging_strdup (const char *s, const char *source_file, int source_line)
{
	char *copy = checking_strdup (s);
	++malloc_count;
	register_ptr (copy, source_file, source_line);
	return copy;
}

void
debugging_free (void *ptr, const char *source_file, int source_line)
{
	/* See checking_free for rationale of this abort.  We repeat it here
	   because we can print the file and the line where the offending
	   free occurred.  */
	if (ptr == NULL)
		DebugConsolePrint( "xfree(NULL) at %s:%d\n", source_file, source_line );
	if (!unregister_ptr (ptr))
		DebugConsolePrint( "bad xfree (0x%0*lx) at %s:%d\n", PTR_FORMAT (ptr), source_file, source_line );

	++free_count;

	checking_free (ptr);
}

#endif /* DEBUG_MALLOC */
